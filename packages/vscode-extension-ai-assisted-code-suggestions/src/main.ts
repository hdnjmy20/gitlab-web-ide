import vscode from 'vscode';
import { GitLabCodeCompletionProvider } from './completion/GitlabCodeCompletionProvider';
import { createStatusBarItem } from './status/CodeSuggestionStatusBarItem';
import { initializeLogging } from './log';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_DISABLE_COMMAND,
  AI_ASSISTED_CODE_SUGGESTIONS_ENABLE_COMMAND,
} from './constants';
import { enableCodeSuggestions, disableCodeSuggestions } from './toggle-setting/toggleSetting';

export async function activate(context: vscode.ExtensionContext) {
  const outputChannel = vscode.window.createOutputChannel('AI assisted code suggestions');
  initializeLogging(line => outputChannel.appendLine(line));

  GitLabCodeCompletionProvider.registerGitLabCodeCompletion(context);

  context.subscriptions.push(
    vscode.commands.registerCommand(
      AI_ASSISTED_CODE_SUGGESTIONS_ENABLE_COMMAND,
      enableCodeSuggestions,
    ),
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      AI_ASSISTED_CODE_SUGGESTIONS_DISABLE_COMMAND,
      disableCodeSuggestions,
    ),
  );

  createStatusBarItem(context);
}
