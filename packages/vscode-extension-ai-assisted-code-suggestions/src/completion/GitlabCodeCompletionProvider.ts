import * as vscode from 'vscode';
import * as path from 'path';
import { log } from '../log';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_API_URL,
  AI_ASSISTED_CODE_SUGGESTIONS_LINES_BELOW_CURSOR,
} from '../constants';
import {
  getAiAssistedCodeSuggestionsConfiguration,
  AI_ASSISTED_CODE_SUGGESTIONS_MODE,
  AI_ASSISTED_CODE_SUGGESTIONS_CONFIG,
} from '../utils/extensionConfiguration';
import { fetchProject } from '../utils/fetchProject';

export class GitLabCodeCompletionProvider implements vscode.InlineCompletionItemProvider {
  private model: string;

  private server: string;

  private debouncedCall: ReturnType<typeof setTimeout> | undefined;

  private debounceTimeMs = 500;

  private noDebounce: boolean;

  constructor(noDebounce = false) {
    this.model = 'gitlab';
    this.server = GitLabCodeCompletionProvider.#getServer();
    this.debouncedCall = undefined;
    this.noDebounce = noDebounce;
  }

  static registerGitLabCodeCompletion(context: vscode.ExtensionContext) {
    let subscription: vscode.Disposable | undefined;
    const dispose = () => subscription?.dispose();
    context.subscriptions.push({ dispose });

    const register = () => {
      subscription = vscode.languages.registerInlineCompletionItemProvider(
        { pattern: '**' },
        new GitLabCodeCompletionProvider(),
      );
    };
    if (getAiAssistedCodeSuggestionsConfiguration().enabled) {
      register();
    }

    vscode.workspace.onDidChangeConfiguration(e => {
      if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_MODE)) {
        if (!getAiAssistedCodeSuggestionsConfiguration().enabled) {
          dispose();
        } else {
          register();
        }
      } else if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_CONFIG)) {
        dispose();
        register();
      }
    });
  }

  static #getServer(): string {
    const serverUrl = new URL(AI_ASSISTED_CODE_SUGGESTIONS_API_URL);
    return serverUrl.href.toString();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async fetchCompletions(params = {}): Promise<any> {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    const response = await fetch(this.server, requestOptions);
    await this.logFetchError(response);

    if (!response.ok) {
      return [];
    }
    const data = await response.json();
    return data;
  }

  async logFetchError(response: Response) {
    if (!response.ok) {
      const body = await response.text().catch(() => undefined);
      log.warn(
        `Fetching code suggestions from ${response.url} failed for server ${this.server}. Body: ${body}`,
      );
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async getPrompt(document: vscode.TextDocument, position: vscode.Position) {
    const contentAboveCursor = document.getText(
      new vscode.Range(0, 0, position.line, position.character),
    );

    const linesBelowCursor = AI_ASSISTED_CODE_SUGGESTIONS_LINES_BELOW_CURSOR;
    const contentBelowCursor = document.getText(
      new vscode.Range(position.line, position.character, position.line + linesBelowCursor, 0),
    );

    const project = await fetchProject();

    const projectPath = project.path_with_namespace || '';
    const projectId = project.id || -1;
    const fileName = path.basename(document.fileName) || '';

    const payload = {
      prompt_version: 1,
      project_path: projectPath,
      project_id: projectId,
      current_file: {
        file_name: fileName,
        content_above_cursor: contentAboveCursor,
        content_below_cursor: contentBelowCursor,
      },
    };

    return payload;
  }

  async getCompletions(document: vscode.TextDocument, position: vscode.Position) {
    const prompt = await this.getPrompt(document, position);

    if (!prompt.current_file.content_above_cursor) {
      log.info('The prompt did not pass validation and was not sent to code suggestion');
      return [] as vscode.InlineCompletionItem[];
    }

    const data = await this.fetchCompletions(prompt);

    return (
      data.choices?.map(
        (choice: { text: string }) =>
          new vscode.InlineCompletionItem(
            choice.text as string,
            new vscode.Range(position, position),
          ),
      ) || []
    );
  }

  async provideInlineCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    context: vscode.InlineCompletionContext,
  ): Promise<vscode.InlineCompletionItem[]> {
    clearTimeout(this.debouncedCall);

    return new Promise(resolve => {
      //  In case of a hover, this will be triggered which is not desired as it calls for a new prediction
      if (context.triggerKind === vscode.InlineCompletionTriggerKind.Automatic) {
        if (this.noDebounce) {
          resolve(this.getCompletions(document, position));
        } else {
          this.debouncedCall = setTimeout(() => {
            resolve(this.getCompletions(document, position));
          }, this.debounceTimeMs);
        }
      }
    });
  }
}
