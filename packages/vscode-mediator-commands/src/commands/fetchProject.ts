import { GitLabClient } from '@gitlab/gitlab-api-client';
import { IFullConfig, GitLabProject } from '../types';

export type FetchProjectCommand = () => Thenable<GitLabProject>;

export const commandFactory =
  (config: IFullConfig, client: GitLabClient): FetchProjectCommand =>
  async () =>
    client.fetchProject(config.projectPath);
