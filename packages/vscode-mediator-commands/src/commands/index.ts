import { GitLabClient } from '@gitlab/gitlab-api-client';
import type {
  BaseConfigLinks,
  PreventUnloadMessage,
  StartRemoteMessage,
  WebIDETrackingMessage,
} from '@gitlab/web-ide-types';
import type { ICommand, IFullConfig, VSBufferWrapper } from '../types';
import * as start from './start';
import * as fetchProject from './fetchProject';
import * as fetchFileRaw from './fetchFileRaw';
import * as commit from './commit';
import { postMessage } from './utils/postMessage';

const PREFIX = 'gitlab-web-ide.mediator';

// region: command names -----------------------------------------------
export const COMMAND_START = `${PREFIX}.start`;
export const COMMAND_FETCH_PROJECT = `${PREFIX}.fetch-project`;
export const COMMAND_FETCH_FILE_RAW = `${PREFIX}.fetch-file-raw`;
export const COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS = `${PREFIX}.fetch-merge-request-diff-stats`;
export const COMMAND_FETCH_PROJECT_BRANCHES = `${PREFIX}.fetch-project-branches`;
export const COMMAND_CREATE_PROJECT_BRANCH = `${PREFIX}.create-project-branch`;
export const COMMAND_READY = `${PREFIX}.ready`;
export const COMMAND_START_REMOTE = `${PREFIX}.start-remote`;
export const COMMAND_OPEN_URI = `${PREFIX}.open-uri`;
export const COMMAND_COMMIT = `${PREFIX}.commit`;
export const COMMAND_PREVENT_UNLOAD = `${PREFIX}.prevent-unload`;
export const COMMAND_SET_HREF = `${PREFIX}.set-href`;
export const COMMAND_TRACK_EVENT = `${PREFIX}.track-event`;

// region: types -------------------------------------------------------
// why: Export these types so that they can be easily consumed by the
//      vscode-extension-web-ide which actually calls these commands
export type StartRemoteMessageParams = StartRemoteMessage['params'];
export type PreventUnloadMessageParams = PreventUnloadMessage['params'];
export type FetchMergeRequestDiffStatsParams = Parameters<
  GitLabClient['fetchMergeRequestDiffStats']
>[0];
export type FetchMergeRequestDiffStatsResponse = Awaited<
  ReturnType<GitLabClient['fetchMergeRequestDiffStats']>
>;
export type FetchProjectBranchesParams = Parameters<GitLabClient['fetchProjectBranches']>[0];
export type FetchProjectBranchesResponse = Awaited<
  ReturnType<GitLabClient['fetchProjectBranches']>
>;
export type CreateProjectBranchParams = Parameters<GitLabClient['createProjectBranch']>[0];
export type CreateProjectBranchResponse = Awaited<ReturnType<GitLabClient['createProjectBranch']>>;

// region: factory function --------------------------------------------
export const createCommands = (config: IFullConfig, bufferWrapper: VSBufferWrapper): ICommand[] => {
  const client = new GitLabClient({
    baseUrl: config.gitlabUrl,
    authToken: config.gitlabToken,
    httpHeaders: config.httpHeaders,
  });

  return [
    {
      id: COMMAND_START,
      handler: start.commandFactory(config, client),
    },
    {
      id: COMMAND_FETCH_PROJECT,
      handler: fetchProject.commandFactory(config, client),
    },
    {
      id: COMMAND_FETCH_FILE_RAW,
      handler: fetchFileRaw.commandFactory(config, client, bufferWrapper),
    },
    {
      id: COMMAND_FETCH_PROJECT_BRANCHES,
      handler: (params: FetchProjectBranchesParams) => client.fetchProjectBranches(params),
    },
    {
      id: COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS,
      handler: (params: FetchMergeRequestDiffStatsParams) =>
        client.fetchMergeRequestDiffStats(params),
    },
    {
      id: COMMAND_CREATE_PROJECT_BRANCH,
      handler: (params: CreateProjectBranchParams) => client.createProjectBranch(params),
    },
    {
      id: COMMAND_READY,
      handler: () => postMessage({ key: 'ready' }),
    },
    {
      // why: Because we are needing to trigger custom behavior from outside the iframe.
      // we'll just post a message from here for the iframe creator to handle.
      id: COMMAND_START_REMOTE,
      handler: (params: StartRemoteMessageParams) => postMessage({ key: 'start-remote', params }),
    },
    {
      id: COMMAND_OPEN_URI,
      handler: ({ key }: { key: keyof BaseConfigLinks }) => {
        const url = config.links[key];

        // We still check for existence because we could be used in a non-typescript environment.
        // TODO: What if the URL wasn't given...
        if (url) {
          window.parent.open(url, '_blank', 'noopener,noreferrer');
        }
      },
    },
    {
      id: COMMAND_COMMIT,
      handler: commit.commandFactory(config, client),
    },
    {
      id: COMMAND_PREVENT_UNLOAD,
      handler: (params: PreventUnloadMessageParams) =>
        postMessage({ key: 'prevent-unload', params }),
    },
    {
      id: COMMAND_TRACK_EVENT,
      handler: (params: WebIDETrackingMessage['params']) =>
        postMessage({ key: 'web-ide-tracking', params }),
    },
    {
      id: COMMAND_SET_HREF,
      handler: (href: string) => {
        const parentHref = window.parent.location.href;
        const newUrl = new URL(href, parentHref);

        window.parent.location.href = newUrl.href;
      },
    },
  ];
};
